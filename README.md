# Backend part

Solution uses H2 in-memory database which is automatically initiated. Database structure and initial data could be found under the resources folder.

## Requirements

Maven 3

Java 11 (Application tested on AdoptOpenJdk 11.0.6+10)

## Build

You can build with included mvnw or with installed maven 3 with JDK 11 in path/JAVA_HOME

## Running

### Using maven

``spring-boot:run``

### Build version

``java -jar target/backend-0.0.1-SNAPSHOT.jar``

### Properties

Can be applied all spring webflux properties.

## What is missing?

1) Normal error reporting what actually happened
2) More data validation
3) Proper Logging!!!