package ee.deniss.helmes.test.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = HelmesApplication.class)
public class HelmesApplication {

  public static void main(String[] args) {
    SpringApplication.run(HelmesApplication.class, args);
  }
}
