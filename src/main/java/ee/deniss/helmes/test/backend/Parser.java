package ee.deniss.helmes.test.backend;

public class Parser {

  static String level2 = "&nbsp;&nbsp;&nbsp;&nbsp;";
  static String level3 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

  static String value =
      "<option value=\"1\">Manufacturing</option>\n"
          + "\t<option value=\"19\">&nbsp;&nbsp;&nbsp;&nbsp;Construction materials</option>\n"
          + "\t<option value=\"18\">&nbsp;&nbsp;&nbsp;&nbsp;Electronics and Optics</option>\n"
          + "\t<option value=\"6\">&nbsp;&nbsp;&nbsp;&nbsp;Food and Beverage</option>\n"
          + "\t<option value=\"342\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bakery &amp; confectionery products</option>\n"
          + "\t<option value=\"43\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beverages</option>\n"
          + "\t<option value=\"42\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fish &amp; fish products </option>\n"
          + "\t<option value=\"40\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Meat &amp; meat products</option>\n"
          + "\t<option value=\"39\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Milk &amp; dairy products </option>\n"
          + "\t<option value=\"437\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other</option>\n"
          + "\t<option value=\"378\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sweets &amp; snack food</option>\n"
          + "\t<option value=\"13\">&nbsp;&nbsp;&nbsp;&nbsp;Furniture</option>\n"
          + "\t<option value=\"389\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bathroom/sauna </option>\n"
          + "\t<option value=\"385\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bedroom</option>\n"
          + "\t<option value=\"390\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Childrenâ€™s room </option>\n"
          + "\t<option value=\"98\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kitchen </option>\n"
          + "\t<option value=\"101\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Living room </option>\n"
          + "\t<option value=\"392\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Office</option>\n"
          + "\t<option value=\"394\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other (Furniture)</option>\n"
          + "\t<option value=\"341\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outdoor </option>\n"
          + "\t<option value=\"99\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project furniture</option>\n"
          + "\t<option value=\"12\">&nbsp;&nbsp;&nbsp;&nbsp;Machinery</option>\n"
          + "\t<option value=\"94\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machinery components</option>\n"
          + "\t<option value=\"91\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machinery equipment/tools</option>\n"
          + "\t<option value=\"224\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manufacture of machinery </option>\n"
          + "\t<option value=\"97\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maritime</option>\n"
          + "\t<option value=\"271\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aluminium and steel workboats </option>\n"
          + "\t<option value=\"269\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Boat/Yacht building</option>\n"
          + "\t<option value=\"230\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ship repair and conversion</option>\n"
          + "\t<option value=\"93\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal structures</option>\n"
          + "\t<option value=\"508\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other</option>\n"
          + "\t<option value=\"227\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Repair and maintenance service</option>\n"
          + "\t<option value=\"11\">&nbsp;&nbsp;&nbsp;&nbsp;Metalworking</option>\n"
          + "\t<option value=\"67\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Construction of metal structures</option>\n"
          + "\t<option value=\"263\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Houses and buildings</option>\n"
          + "\t<option value=\"267\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal products</option>\n"
          + "\t<option value=\"542\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal works</option>\n"
          + "\t<option value=\"75\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CNC-machining</option>\n"
          + "\t<option value=\"62\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Forgings, Fasteners </option>\n"
          + "\t<option value=\"69\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gas, Plasma, Laser cutting</option>\n"
          + "\t<option value=\"66\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIG, TIG, Aluminum welding</option>\n"
          + "\t<option value=\"9\">&nbsp;&nbsp;&nbsp;&nbsp;Plastic and Rubber</option>\n"
          + "\t<option value=\"54\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Packaging</option>\n"
          + "\t<option value=\"556\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic goods</option>\n"
          + "\t<option value=\"559\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic processing technology</option>\n"
          + "\t<option value=\"55\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blowing</option>\n"
          + "\t<option value=\"57\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moulding</option>\n"
          + "\t<option value=\"53\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastics welding and processing</option>\n"
          + "\t<option value=\"560\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic profiles</option>\n"
          + "\t<option value=\"5\">&nbsp;&nbsp;&nbsp;&nbsp;Printing </option>\n"
          + "\t<option value=\"148\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Advertising</option>\n"
          + "\t<option value=\"150\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Book/Periodicals printing</option>\n"
          + "\t<option value=\"145\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Labelling and packaging printing</option>\n"
          + "\t<option value=\"7\">&nbsp;&nbsp;&nbsp;&nbsp;Textile and Clothing</option>\n"
          + "\t<option value=\"44\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing</option>\n"
          + "\t<option value=\"45\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Textile</option>\n"
          + "\t<option value=\"8\">&nbsp;&nbsp;&nbsp;&nbsp;Wood</option>\n"
          + "\t<option value=\"337\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other (Wood)</option>\n"
          + "\t<option value=\"51\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wooden building materials</option>\n"
          + "\t<option value=\"47\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wooden houses</option>\n"
          + "\t<option value=\"3\">Other</option>\n"
          + "\t<option value=\"37\">&nbsp;&nbsp;&nbsp;&nbsp;Creative industries</option>\n"
          + "\t<option value=\"29\">&nbsp;&nbsp;&nbsp;&nbsp;Energy technology</option>\n"
          + "\t<option value=\"33\">&nbsp;&nbsp;&nbsp;&nbsp;Environment</option>\n"
          + "\t<option value=\"2\">Service</option>\n"
          + "\t<option value=\"25\">&nbsp;&nbsp;&nbsp;&nbsp;Business services</option>\n"
          + "\t<option value=\"35\">&nbsp;&nbsp;&nbsp;&nbsp;Engineering</option>\n"
          + "\t<option value=\"28\">&nbsp;&nbsp;&nbsp;&nbsp;Information Technology and Telecommunications</option>\n"
          + "\t<option value=\"581\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data processing, Web portals, E-marketing</option>\n"
          + "\t<option value=\"576\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Programming, Consultancy</option>\n"
          + "\t<option value=\"121\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Software, Hardware</option>\n"
          + "\t<option value=\"122\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telecommunications</option>\n"
          + "\t<option value=\"22\">&nbsp;&nbsp;&nbsp;&nbsp;Tourism</option>\n"
          + "\t<option value=\"141\">&nbsp;&nbsp;&nbsp;&nbsp;Translation services</option>\n"
          + "\t<option value=\"21\">&nbsp;&nbsp;&nbsp;&nbsp;Transport and Logistics</option>\n"
          + "\t<option value=\"111\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Air</option>\n"
          + "\t<option value=\"114\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rail</option>\n"
          + "\t<option value=\"112\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Road</option>\n"
          + "\t<option value=\"113\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Water</option>";

  public static void main(String[] args) {

    String[] split = value.split("\n");

    String lastRootId = "0";

    String previousId = "";
    Integer lastLevel = 0;
    Integer previousLevel = 0;

    for (String s : split) {

      s = s.replace("</option>", "").replace("\t", "").replace("<option value=\"", "");

      String[] split2 = s.split("\">");

      previousId = split2[0];
      previousLevel = lastLevel;

      if (!split2[1].contains(level2)) {
        lastLevel = 1;
        lastRootId = split2[0];

        System.out.printf("(%s, NULL, '%s'), \n", split2[0], split2[1]);

      } else if (split2[1].contains(level2)) {
        lastLevel = 3;

        System.out.printf(
            "(%s, %s,'%s'), \n", split2[0], lastRootId, split2[1].replace(level2, ""));
      } else {
        lastLevel = 1;
        System.out.printf(
            "(%s, %s,'%s'), \n", split2[0], lastRootId, split2[1].replace(level2, ""));
      }

      if (!lastLevel.equals(previousLevel)) {
        lastRootId = previousId;
      }
    }
  }
}
