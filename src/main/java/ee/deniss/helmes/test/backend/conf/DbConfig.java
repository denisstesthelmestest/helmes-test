package ee.deniss.helmes.test.backend.conf;

import io.r2dbc.h2.H2ConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.r2dbc.connectionfactory.init.ConnectionFactoryInitializer;
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

/** R2DBC database driver spring configuration for H2 in-memory database */
@Configuration
@EnableR2dbcRepositories
public class DbConfig {

  /** @return H2 database implementation of {@link ConnectionFactory} */
  @Bean
  public ConnectionFactory connectionFactory() {
    return H2ConnectionFactory.inMemory("inner-database");
  }

  /**
   * Startup bean for H2 database for prefilling database
   *
   * @param connectionFactory H2 instance of {@link ConnectionFactory}
   * @return bean for h2 database data population
   */
  @Bean
  public ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {
    var initializer = new ConnectionFactoryInitializer();
    initializer.setConnectionFactory(connectionFactory);
    initializer.setDatabasePopulator(
        new ResourceDatabasePopulator(new ClassPathResource("init.sql")));
    return initializer;
  }
}
