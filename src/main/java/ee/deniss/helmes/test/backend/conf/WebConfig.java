package ee.deniss.helmes.test.backend.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.reactive.config.EnableWebFlux;

/** WebFlux configuration spring component */
@Configuration
@EnableWebFlux
public class WebConfig {

  /**
   * CORS settings bean
   *
   * @return instance of {@link CorsWebFilter}
   */
  @Bean
  @Order(Ordered.HIGHEST_PRECEDENCE)
  CorsWebFilter corsWebFilter() {
    var corsConfig = new CorsConfiguration();
    corsConfig.applyPermitDefaultValues();

    var source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", corsConfig);

    return new CorsWebFilter(source);
  }
}
