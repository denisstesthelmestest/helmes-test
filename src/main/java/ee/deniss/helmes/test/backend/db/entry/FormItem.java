package ee.deniss.helmes.test.backend.db.entry;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/** Entry for {@literal ITEMS} table in database */
@Table(FormItem.TABLE_NAME)
public class FormItem {

  static final String TABLE_NAME = "ITEMS";

  @Id private Long id;

  @NotNull
  @NotEmpty
  @Max(value = 250)
  private String name;

  @NotNull private Long selectorId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getSelectorId() {
    return selectorId;
  }

  public void setSelectorId(Long selectorId) {
    this.selectorId = selectorId;
  }
}
