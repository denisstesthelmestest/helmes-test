package ee.deniss.helmes.test.backend.db.entry;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/** Entry for {@literal SELECTOR_ITEMS} table in database */
@Table(SelectorItem.TABLE_NAME)
public class SelectorItem {

  static final String TABLE_NAME = "SELECTOR_ITEMS";

  @Id private Long id;
  private Long parentId;

  @NotNull
  @NotEmpty
  @Max(value = 256)
  private String name;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
