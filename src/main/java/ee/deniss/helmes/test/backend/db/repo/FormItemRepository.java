package ee.deniss.helmes.test.backend.db.repo;

import ee.deniss.helmes.test.backend.db.entry.FormItem;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

/** {@link FormItem} entry repository */
@Repository
public interface FormItemRepository extends R2dbcRepository<FormItem, Long> {}
