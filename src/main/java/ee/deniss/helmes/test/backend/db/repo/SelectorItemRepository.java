package ee.deniss.helmes.test.backend.db.repo;

import ee.deniss.helmes.test.backend.db.entry.SelectorItem;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

/** {@link SelectorItem} entry repository */
@Repository
public interface SelectorItemRepository extends R2dbcRepository<SelectorItem, Long> {}
