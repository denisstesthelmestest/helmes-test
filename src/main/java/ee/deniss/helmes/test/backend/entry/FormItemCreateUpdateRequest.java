package ee.deniss.helmes.test.backend.entry;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/** Item entry for rest service response */
public class FormItemCreateUpdateRequest {
  @Size(min = 2, max = 250)
  private String name;

  @NotNull private Long selectorId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getSelectorId() {
    return selectorId;
  }

  public void setSelectorId(Long selectorId) {
    this.selectorId = selectorId;
  }
}
