package ee.deniss.helmes.test.backend.entry;

import ee.deniss.helmes.test.backend.db.entry.FormItem;

/** Form Item entry for rest service response */
public class FormItemResponse {

  private Long id;
  private String name;
  private Long selectorId;

  /**
   * Transforms {@link FormItem} to {@link FormItemResponse}
   *
   * @param formItem item to transform
   * @return transformed item
   */
  public static FormItemResponse fromSelectorItem(FormItem formItem) {
    var itemResponse = new FormItemResponse();

    itemResponse.setId(formItem.getId());
    itemResponse.setName(formItem.getName());
    itemResponse.setSelectorId(formItem.getSelectorId());

    return itemResponse;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getSelectorId() {
    return selectorId;
  }

  public void setSelectorId(Long selectorId) {
    this.selectorId = selectorId;
  }
}
