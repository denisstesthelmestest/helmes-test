package ee.deniss.helmes.test.backend.entry;

import ee.deniss.helmes.test.backend.db.entry.SelectorItem;

import java.util.List;

/** Selection list entry for rest-service response */
public class SelectorItemResponse {

  private Long id;
  private String name;
  private Long parentId;

  private List<SelectorItemResponse> child;

  /**
   * Transforms {@link SelectorItem} to {@link SelectorItemResponse}
   *
   * @param selectorItem item to transform
   * @return transformed item
   */
  public static SelectorItemResponse fromSelectorItem(SelectorItem selectorItem) {
    var selectorItemResponse = new SelectorItemResponse();

    selectorItemResponse.setId(selectorItem.getId());
    selectorItemResponse.setName(selectorItem.getName());
    selectorItemResponse.setParentId(selectorItem.getParentId());

    return selectorItemResponse;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public List<SelectorItemResponse> getChild() {
    return child;
  }

  public void setChild(List<SelectorItemResponse> child) {
    this.child = child;
  }

  @Override
  public String toString() {
    return "SelectorItemResponse{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", parentId="
        + parentId
        + ", child="
        + child
        + '}';
  }
}
