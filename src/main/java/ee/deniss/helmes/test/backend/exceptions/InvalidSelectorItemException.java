package ee.deniss.helmes.test.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/** Exception for indicating that some item was not found */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidSelectorItemException extends RuntimeException {}
