package ee.deniss.helmes.test.backend.rest;

import ee.deniss.helmes.test.backend.entry.FormItemCreateUpdateRequest;
import ee.deniss.helmes.test.backend.entry.FormItemResponse;
import ee.deniss.helmes.test.backend.entry.SelectorItemResponse;
import ee.deniss.helmes.test.backend.service.FormItemsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/** Rest-controller for fetching selector items */
@RestController
@RequestMapping("/form-item")
public class FormItemsRest {

  private final FormItemsService itemsService;

  /**
   * Constructor
   *
   * @param itemsService item service instance
   */
  public FormItemsRest(FormItemsService itemsService) {
    this.itemsService = itemsService;
  }

  /**
   * Mapping for GET /form-item/{id} call for getting form item
   *
   * @param id id of item
   * @return flux of {@link SelectorItemResponse}
   */
  @GetMapping("/{id}")
  public Mono<FormItemResponse> getFormItem(@PathVariable Long id) {
    return itemsService.getItemById(id);
  }

  /**
   * Mapping for POST /form-item/{id} call for updating form item data
   *
   * @param id id of item
   * @param request update request data
   * @return Updated {@link SelectorItemResponse}
   */
  @PostMapping("/{id}")
  public Mono<FormItemResponse> updateFormItem(
      @PathVariable Long id, @Valid @RequestBody FormItemCreateUpdateRequest request) {
    return itemsService.update(id, request);
  }

  /**
   * Mapping for PUT /form-item call for creating form item
   *
   * @param request create request data
   * @return Created {@link SelectorItemResponse}
   */
  @PutMapping
  public Mono<FormItemResponse> createFormItem(
    @Valid @RequestBody FormItemCreateUpdateRequest request) {
    return itemsService.create(request);
  }
}
