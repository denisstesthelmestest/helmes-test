package ee.deniss.helmes.test.backend.rest;

import ee.deniss.helmes.test.backend.entry.SelectorItemResponse;
import ee.deniss.helmes.test.backend.service.SelectorItemsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/** Rest-controller for fetching selector items */
@RestController
@RequestMapping("/selector-items")
public class SelectorItemsRest {

  private final SelectorItemsService selectorItemsService;

  /**
   * Constructor
   *
   * @param selectorItemsService service for fetching selector items
   */
  public SelectorItemsRest(SelectorItemsService selectorItemsService) {
    this.selectorItemsService = selectorItemsService;
  }

  /**
   * Mapping for GET /selector-items call for getting all items
   *
   * @return flux of {@link SelectorItemResponse}
   */
  @GetMapping
  public Flux<SelectorItemResponse> getAllSelectorItems() {
    return selectorItemsService.getAllItems();
  }
}
