package ee.deniss.helmes.test.backend.service;

import ee.deniss.helmes.test.backend.db.entry.FormItem;
import ee.deniss.helmes.test.backend.db.repo.FormItemRepository;
import ee.deniss.helmes.test.backend.entry.FormItemCreateUpdateRequest;
import ee.deniss.helmes.test.backend.entry.FormItemResponse;
import ee.deniss.helmes.test.backend.exceptions.InvalidSelectorItemException;
import ee.deniss.helmes.test.backend.exceptions.NotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/** Service for storing/editing with form items */
@Service
public class FormItemsService {

  private final FormItemRepository formItemRepository;
  private final SelectorItemsService selectorItemsService;

  /**
   * Constructor
   *
   * @param formItemRepository form items repository
   * @param selectorItemsService selector item service instance
   */
  public FormItemsService(
      FormItemRepository formItemRepository, SelectorItemsService selectorItemsService) {
    this.formItemRepository = formItemRepository;
    this.selectorItemsService = selectorItemsService;
  }

  /**
   * Fetches item by id
   *
   * @param id item id to be fetched
   * @return found item
   */
  public Mono<FormItemResponse> getItemById(Long id) {
    return formItemRepository
        .findById(id)
        .switchIfEmpty(Mono.error(new NotFoundException()))
        .map(FormItemResponse::fromSelectorItem);
  }

  /**
   * Creates Form item using request data
   *
   * @param request request data
   * @return newly created form item
   */
  public Mono<FormItemResponse> create(FormItemCreateUpdateRequest request) {
    return selectorItemsService
        .existsById(request.getSelectorId())
        .flatMap(
            exists -> {
              if (!exists) {
                return Mono.error(new InvalidSelectorItemException());
              }

              var item = new FormItem();
              item.setName(request.getName());
              item.setSelectorId(request.getSelectorId());

              return formItemRepository.save(item).map(FormItemResponse::fromSelectorItem);
            });
  }

  /**
   * Updates existing form item
   *
   * @param id id of form item
   * @param request update request
   * @return updated form item
   */
  public Mono<FormItemResponse> update(Long id, FormItemCreateUpdateRequest request) {
    return selectorItemsService
        .existsById(request.getSelectorId())
        .flatMap(
            exists -> {
              if (!exists) {
                return Mono.error(new InvalidSelectorItemException());
              }

              return formItemRepository
                  .findById(id)
                  .switchIfEmpty(Mono.error(new NotFoundException()))
                  .flatMap(
                      i -> {
                        i.setName(request.getName());
                        i.setSelectorId(request.getSelectorId());
                        return formItemRepository.save(i);
                      })
                  .map(FormItemResponse::fromSelectorItem);
            });
  }
}
