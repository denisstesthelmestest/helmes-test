package ee.deniss.helmes.test.backend.service;

import ee.deniss.helmes.test.backend.db.repo.SelectorItemRepository;
import ee.deniss.helmes.test.backend.entry.SelectorItemResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/** Service for gathering data selectors from database */
@Service
public class SelectorItemsService {

  private final SelectorItemRepository selectorItemRepository;

  /**
   * Constructor
   *
   * @param selectorItemRepository Selector item repository instance
   */
  public SelectorItemsService(SelectorItemRepository selectorItemRepository) {
    this.selectorItemRepository = selectorItemRepository;
  }

  /**
   * Fetches all selector items and maps to tree response
   *
   * @return list of {@link SelectorItemRepository}
   */
  public Flux<SelectorItemResponse> getAllItems() {
    return selectorItemRepository
        .findAll()
        .map(SelectorItemResponse::fromSelectorItem)
        .collectList()
        .flatMap(items -> Mono.fromCallable(() -> findChildrenItemsById(null, items)))
        .flatMapMany(Flux::fromIterable);
  }

  /**
   * Checks that selector item exists in database
   *
   * @param id selector item id to check
   * @return {@code true} if item exists in database
   */
  public Mono<Boolean> existsById(Long id) {
    return selectorItemRepository.existsById(id);
  }

  private List<SelectorItemResponse> findChildrenItemsById(
      Long parentId, List<SelectorItemResponse> originalItems) {
    return originalItems.stream()
        .filter(i -> i.getId() != null)
        .filter(i -> Objects.equals(parentId, i.getParentId()))
        .peek(i -> i.setChild(findChildrenItemsById(i.getId(), originalItems)))
        .collect(Collectors.toList());
  }
}
