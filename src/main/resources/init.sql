DROP TABLE IF EXISTS SELECTOR_ITEMS;

CREATE TABLE SELECTOR_ITEMS (
  id INT PRIMARY KEY,
  parent_id INT,
  name VARCHAR(250) NOT NULL
);

INSERT INTO SELECTOR_ITEMS (id, parent_id, name) VALUES
(1, NULL, 'Manufacturing'),
(19, 1,'Construction materials'),
(18, 19,'Electronics and Optics'),
(6, 19,'Food and Beverage'),
(342, 19,'Bakery & confectionery products'),
(43, 19,'Beverages'),
(42, 19,'Fish & fish products '),
(40, 19,'Meat & meat products'),
(39, 19,'Milk & dairy products '),
(437, 19,'Other'),
(378, 19,'Sweets & snack food'),
(13, 19,'Furniture'),
(389, 19,'Bathroom/sauna '),
(385, 19,'Bedroom'),
(390, 19,'Childrenâ€™s room '),
(98, 19,'Kitchen '),
(101, 19,'Living room '),
(392, 19,'Office'),
(394, 19,'Other (Furniture)'),
(341, 19,'Outdoor '),
(99, 19,'Project furniture'),
(12, 19,'Machinery'),
(94, 19,'Machinery components'),
(91, 19,'Machinery equipment/tools'),
(224, 19,'Manufacture of machinery '),
(97, 19,'Maritime'),
(271, 19,'Aluminium and steel workboats '),
(269, 19,'Boat/Yacht building'),
(230, 19,'Ship repair and conversion'),
(93, 19,'Metal structures'),
(508, 19,'Other'),
(227, 19,'Repair and maintenance service'),
(11, 19,'Metalworking'),
(67, 19,'Construction of metal structures'),
(263, 19,'Houses and buildings'),
(267, 19,'Metal products'),
(542, 19,'Metal works'),
(75, 19,'CNC-machining'),
(62, 19,'Forgings, Fasteners '),
(69, 19,'Gas, Plasma, Laser cutting'),
(66, 19,'MIG, TIG, Aluminum welding'),
(9, 19,'Plastic and Rubber'),
(54, 19,'Packaging'),
(556, 19,'Plastic goods'),
(559, 19,'Plastic processing technology'),
(55, 19,'Blowing'),
(57, 19,'Moulding'),
(53, 19,'Plastics welding and processing'),
(560, 19,'Plastic profiles'),
(5, 19,'Printing '),
(148, 19,'Advertising'),
(150, 19,'Book/Periodicals printing'),
(145, 19,'Labelling and packaging printing'),
(7, 19,'Textile and Clothing'),
(44, 19,'Clothing'),
(45, 19,'Textile'),
(8, 19,'Wood'),
(337, 19,'Other (Wood)'),
(51, 19,'Wooden building materials'),
(47, 19,'Wooden houses'),
(3, NULL, 'Other'),
(37, 3,'Creative industries'),
(29, 37,'Energy technology'),
(33, 37,'Environment'),
(2, NULL, 'Service'),
(25, 2,'Business services'),
(35, 25,'Engineering'),
(28, 25,'Information Technology and Telecommunications'),
(581, 25,'Data processing, Web portals, E-marketing'),
(576, 25,'Programming, Consultancy'),
(121, 25,'Software, Hardware'),
(122, 25,'Telecommunications'),
(22, 25,'Tourism'),
(141, 25,'Translation services'),
(21, 25,'Transport and Logistics'),
(111, 25,'Air'),
(114, 25,'Rail'),
(112, 25,'Road'),
(113, 25,'Water');

DROP TABLE IF EXISTS ITEMS;

CREATE TABLE ITEMS (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  selector_id INT
);
