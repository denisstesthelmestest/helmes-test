package ee.deniss.helmes.test.backend.rest;

import ee.deniss.helmes.test.backend.HelmesApplication;
import ee.deniss.helmes.test.backend.db.entry.FormItem;
import ee.deniss.helmes.test.backend.db.repo.FormItemRepository;
import ee.deniss.helmes.test.backend.entry.FormItemResponse;
import ee.deniss.helmes.test.backend.service.FormItemsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = HelmesApplication.class)
@WebFluxTest(FormItemsRest.class)
@Import({FormItemsService.class})
class FormItemsRestTest {

  @Autowired WebTestClient client;

  @MockBean private FormItemRepository formItemRepositoryMock;

  @Test
  void getFormItem_notFound() {
    when(formItemRepositoryMock
           .findById(1L)).thenReturn(Mono.empty());

    client.get()
      .uri("/form-item/1")
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  void getFormItem() {
    var formItem = new FormItem();
    formItem.setId(1L);
    formItem.setName("Test");
    formItem.setSelectorId(10L);

    when(formItemRepositoryMock
           .findById(1L)).thenReturn(Mono.just(formItem));

    var response = client.get()
                                      .uri("/form-item/1")
                                      .exchange()
                                      .expectStatus()
                                      .isOk()
                                      .expectBody(FormItemResponse.class)
                                      .returnResult()
                                      .getResponseBody();


    assertNotNull(response);
    assertEquals("Test", response.getName());
    assertEquals(1L, response.getId());
    assertEquals(10L, response.getSelectorId());
  }

  // other tests here........... low on time.
}
