package ee.deniss.helmes.test.backend.rest;

import ee.deniss.helmes.test.backend.HelmesApplication;
import ee.deniss.helmes.test.backend.db.entry.SelectorItem;
import ee.deniss.helmes.test.backend.db.repo.SelectorItemRepository;
import ee.deniss.helmes.test.backend.entry.SelectorItemResponse;
import ee.deniss.helmes.test.backend.service.SelectorItemsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = HelmesApplication.class)
@WebFluxTest(SelectorItemsRest.class)
@Import({
  SelectorItemsService.class
})
class SelectorFormItemsRestTest {

  @Autowired
  WebTestClient client;

  @MockBean
  SelectorItemRepository selectorItemRepositoryMock;

  @Test
  void getAllSelectorItems() {
    var item1 = new SelectorItem();
    item1.setId(1L);
    item1.setName("First");

    var item2 = new SelectorItem();
    item2.setId(2L);
    item2.setName("Second");

    when(selectorItemRepositoryMock.findAll())
      .thenReturn(Flux.just(item1, item2));

    client.get()
      .uri("/selector-items")
      .exchange()
      .expectStatus()
      .isOk()
      .expectBodyList(SelectorItemResponse.class)
      .hasSize(2);
  }
}