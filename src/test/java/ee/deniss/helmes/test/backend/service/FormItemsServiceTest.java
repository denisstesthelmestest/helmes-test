package ee.deniss.helmes.test.backend.service;

import ee.deniss.helmes.test.backend.db.entry.FormItem;
import ee.deniss.helmes.test.backend.db.repo.FormItemRepository;
import ee.deniss.helmes.test.backend.entry.FormItemCreateUpdateRequest;
import ee.deniss.helmes.test.backend.exceptions.InvalidSelectorItemException;
import ee.deniss.helmes.test.backend.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FormItemsServiceTest {

  private FormItemRepository formItemRepositoryMock;
  private SelectorItemsService selectorItemsServiceMock;

  private FormItemsService formItemsService;

  @BeforeEach
  void setUp() {
    formItemRepositoryMock = mock(FormItemRepository.class);
    selectorItemsServiceMock = mock(SelectorItemsService.class);

    formItemsService = new FormItemsService(formItemRepositoryMock, selectorItemsServiceMock);
  }

  @Test
  void getItemById_notFound() {
    when(formItemRepositoryMock.findById(eq(10L))).thenReturn(Mono.empty());

    StepVerifier.create(formItemsService.getItemById(10L))
        .expectErrorMatches(e -> e instanceof NotFoundException)
        .verify();
  }

  @Test
  void getItemById() {
    var item = new FormItem();
    item.setId(10L);
    item.setSelectorId(20L);
    item.setName("Item name 1");

    when(formItemRepositoryMock.findById(eq(10L))).thenReturn(Mono.just(item));

    StepVerifier.create(formItemsService.getItemById(10L))
        .expectNextMatches(
            i ->
                i.getId().equals(10L)
                    && i.getSelectorId().equals(20L)
                    && i.getName().equals("Item name 1"))
        .verifyComplete();
  }

  @Test
  void create_selectorNotExists() {
    when(selectorItemsServiceMock.existsById(eq(10L))).thenReturn(Mono.just(false));

    var item = new FormItemCreateUpdateRequest();
    item.setSelectorId(10L);

    StepVerifier.create(formItemsService.create(item))
        .expectErrorMatches(e -> e instanceof InvalidSelectorItemException)
        .verify();
  }

  @Test
  void create() {
    var item = new FormItem();
    item.setName("result");
    when(selectorItemsServiceMock.existsById(eq(10L))).thenReturn(Mono.just(true));
    when(formItemRepositoryMock.save(
            argThat(
                a ->
                    a.getId() == null
                        && a.getName().equals("Name Item")
                        && a.getSelectorId().equals(10L))))
        .thenReturn(Mono.just(item));

    var requestItem = new FormItemCreateUpdateRequest();
    requestItem.setName("Name Item");
    requestItem.setSelectorId(10L);

    StepVerifier.create(formItemsService.create(requestItem))
        .expectNextMatches(i -> i.getName().equals("result"))
        .verifyComplete();
  }

  @Test
  void update_selectorNotExists() {
    when(selectorItemsServiceMock.existsById(eq(10L))).thenReturn(Mono.just(false));

    var item = new FormItemCreateUpdateRequest();
    item.setSelectorId(10L);

    StepVerifier.create(formItemsService.update(1L, item))
      .expectErrorMatches(e -> e instanceof InvalidSelectorItemException)
      .verify();
  }

  @Test
  void update_notFound() {
    var item = new FormItem();
    item.setId(1L);
    item.setName("Name Item");
    when(selectorItemsServiceMock.existsById(eq(10L))).thenReturn(Mono.just(true));
    when(formItemRepositoryMock.findById(eq(1L))).thenReturn(Mono.empty());

    var requestItem = new FormItemCreateUpdateRequest();
    requestItem.setName("Name Item");
    requestItem.setSelectorId(10L);

    StepVerifier.create(formItemsService.update(1L, requestItem))
      .expectErrorMatches(e -> e instanceof NotFoundException)
      .verify();
  }

  @Test
  void update() {
    var item = new FormItem();
    item.setId(1L);
    item.setName("Name Item");
    when(selectorItemsServiceMock.existsById(eq(10L))).thenReturn(Mono.just(true));

    when(formItemRepositoryMock.findById(eq(1L))).thenReturn(Mono.just(item));
    when(formItemRepositoryMock.save(
      argThat(
        a ->
          a.getId() == 1L
            && a.getName().equals("Name Item")
            && a.getSelectorId().equals(10L))))
      .thenReturn(Mono.just(item));

    var requestItem = new FormItemCreateUpdateRequest();
    requestItem.setName("Name Item");
    requestItem.setSelectorId(10L);

    StepVerifier.create(formItemsService.update(1L, requestItem))
      .expectNextMatches(i -> i.getName().equals("Name Item"))
      .verifyComplete();
  }
}
