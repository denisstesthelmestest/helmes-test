package ee.deniss.helmes.test.backend.service;

import ee.deniss.helmes.test.backend.db.entry.SelectorItem;
import ee.deniss.helmes.test.backend.db.repo.SelectorItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SelectorItemsServiceTest {

  private SelectorItemsService service;
  private SelectorItemRepository selectorItemRepositoryMock;

  @BeforeEach
  void setUp() {
    selectorItemRepositoryMock = mock(SelectorItemRepository.class);
    service = new SelectorItemsService(selectorItemRepositoryMock);
  }

  @Test
  void getAllItems_empty() {
    when(selectorItemRepositoryMock.findAll())
      .thenReturn(Flux.just());

    StepVerifier
      .create(service.getAllItems())
      .expectNextCount(0)
      .verifyComplete();
  }

  @Test
  void getAllItems_noStackOverflowEmptyId() {
    when(selectorItemRepositoryMock.findAll())
      .thenReturn(Flux.just(new SelectorItem()));

    StepVerifier
      .create(service.getAllItems())
      .expectNextCount(0)
      .verifyComplete();
  }

  @Test
  void getAllItems() {
    var item1 = new SelectorItem();
    item1.setId(1L);
    item1.setName("First");

    var item2 = new SelectorItem();
    item2.setId(2L);
    item2.setName("Second");

    var item11 = new SelectorItem();
    item11.setId(11L);
    item11.setParentId(1L);
    item11.setName("First.First");

    var item12 = new SelectorItem();
    item12.setId(12L);
    item12.setParentId(1L);
    item12.setName("First.Second");

    var item113 = new SelectorItem();
    item113.setId(111L);
    item113.setParentId(11L);
    item113.setName("First.First.First");

    when(selectorItemRepositoryMock.findAll())
      .thenReturn(Flux.just(item1, item2, item11, item12, item113));

    StepVerifier
      .create(service.getAllItems())
      .expectNextMatches(i -> {
        if (!i.getId().equals(1L) || !i.getName().equals("First") || i.getParentId() != null || i.getChild().size() != 2) {
          return false;
        }

        var c1 = i.getChild().get(0);

        if (!c1.getId().equals(11L) || !c1.getName().equals("First.First") || c1.getParentId() != 1L || c1.getChild().size() != 1) {
          return false;
        }

        var c11 = c1.getChild().get(0);

        if (!c11.getId().equals(111L) || !c11.getName().equals("First.First.First") || c11.getParentId() != 11L || !c11.getChild().isEmpty()) {
          return false;
        }

        var c2 = i.getChild().get(1);

        return c2.getId().equals(12L) && c2.getName().equals("First.Second") && c2.getParentId() == 1L && c2.getChild().isEmpty();
      })
      .expectNextMatches(i -> i.getId().equals(2L) && i.getName().equals("Second") && i.getParentId() == null && i.getChild().isEmpty())
      .verifyComplete();
  }

  @Test
  void existsById() {
    when(selectorItemRepositoryMock.existsById(anyLong())).thenReturn(Mono.just(false));
    when(selectorItemRepositoryMock.existsById(eq(20L))).thenReturn(Mono.just(true));

    StepVerifier
      .create(service.existsById(20L))
      .expectNext(true)
      .verifyComplete();

    StepVerifier
      .create(service.existsById(21L))
      .expectNext(false)
      .verifyComplete();
  }
}